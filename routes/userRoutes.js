const express = require("express");
const router = express.Router();

const userController = require("../controllers/userControllers");
	// Route for cheking Email
	router.post("/checkEmail", userController.checkEmailExists);
	// Route for registration
	router.post("/register", userController.registerUser);

	router.post("/login", userController.loginUser);

	router.post("/userId", userController.checkUserId);

module.exports = router;