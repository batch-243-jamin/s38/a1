// Setup dependencies

const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const app = express();

const userRoutes = require("./routes/userRoutes");
// Allows all resources to access our backend application
app.use(cors())
app.use(express());
// express.json( fuction is a built-in middleware function in express. It parses incoming request with JSON payloads.)
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "/users" to be included for all user routes defined in the
app.use("/users", userRoutes);
// Database connection

mongoose.connect("mongodb+srv://admin:admin@batch243jamin.kuyagcd.mongodb.net/courseBookingAPI?retryWrites=true&w=majority",
			{
				useNewUrlParser: true,
				useUnifiedTopology: true
			});

	mongoose.connection.on("error", console.error.bind(console, "connection error"));
	mongoose.connection.once(`open`, () => console.log("Now connected to MongoDB Atlas"))


// This syntax will allow flexibility when using the application both locally or as a hosted app
// process.evn.PORT is that can be assigned by your hosting service
app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT||4000}`);
});