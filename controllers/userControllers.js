const User = require("../models/User");
const bcrypt = require("bcrypt");

// Check if email already exists
/*
	Steps:
	1. Use mongoose "find" method to find duplicate emails.
	2. Use the "then" method to send a response to the frontend application based on the result of the find method.
*/
module.exports.checkEmailExists = (request, response) => {
	// The result is sent back to the frontend via the then method
	return User.find({email:request.body.email}).then(result => {
		console.log(request.body.email)
		let message = ``;
		// find method returns an array record of matching documents
		if(result.length > 0){
			message = `The ${request.body.email} is already taken, please use other email.`
			return response.send(message);
		}
		// No duplicate email found
		// The email is not yet registered in the database.
		else{
			message = `That the email: ${request.body.email} is not yet taken.`
			return response.send(message);
		}
	})
}


module.exports.registerUser = (request, response) => {

	// creates variable "newUser" and instantiates a new 'User' object using mongoose model
	// Uses the information from request body to provide necessary information.
	let newUser = new User({
		_id: request.body._id,
		firstName: request.body.firstName,
		lastName: request.body.lastName,
		email: request.body.email,
		/*salt - salt rounds that bcrypt algorithm will run to encrypt the password*/
		password: bcrypt.hashSync(request.body.password, 10),
		mobileNo: request.body.mobileNo
	})

	// Saves the created object to our database.
	return newUser.save().then(user => {
		console.log(user);
		response.send(`Congratulations, Sir/Ma'am ${newUser.firstName}! You are now registered.`)
	}).catch(error =>{
		console.log(error);
	response.send(`Sorry ${newUser.firstName}, there was an error during the registration. Please try again!`);
	} )
	
}
// User Authentication
/*
	Steps:
		1. Check database if the user email exist.
		2. Compare the password provided in the login with the password stored in the database.
*/
module.exports.loginUser = (request, response) => {
	// The findOne method, returns the first record in the collection that matches the search criteria.

	return User.findOne({email: request.body.email}).then(result => {
		console.log(result);
		if(result === null){
			response.send(`Your email ${request.body.email} is not yet registered. Register first!`)
		}
		else{
				// Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the databasssse. Passwprd.
				// The compareSyn method is used to compare non encrypte password from the login form to the encrypted password retrive. It will return true or false value depending on the result.
			const isPasswordCorrect = bcrypt.compareSync(request.body.password, result.password);
			if(isPasswordCorrect){

				return response.send("Logged in successfully.")
				
			}
			else{
				return response.send("Incorrect Password. Please try again.");
			}
		}
	})
}

module.exports.checkUserId = ( request, response) => {
	return User.findOne({_id: request.body.id}).then(result => {
		if(result === null){
			return response.send(`Id can't be found.`)
		}
		else{
			result.password = "";
			return response.send(result)
		}
	})
}